import { Router } from 'express'
import { validation } from '../controllers/validation.controller'

const router = Router()

router.post('/', validation)

export const ValidationRouter = router
