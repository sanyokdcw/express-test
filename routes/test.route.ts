import { Router } from 'express'
import { test } from '../controllers/test.controller'

const router = Router()

router.post('/', test)

export const TestRouter = router
