/* ==================== LIBRARIES, MIDDLEWARES START ==================== */
import dotenv from 'dotenv'
import express from 'express'
import helmet from 'helmet'
import morgan from 'morgan'
import passport from 'passport'
import xmlParser from 'express-xml-bodyparser'
/* ==================== LIBRARIES, MIDDLEWARES END ==================== */
/*==================== ROUTES IMPORT START ====================*/
import { TestRouter, ValidationRouter } from './routes/'
/*==================== ROUTES IMPORT END ====================*/

/*==================== INIT START ====================*/
const app = express()
dotenv.config()
const { PORT } = process.env

/*==================== MIDDLEWARE IMPORTS END ====================*/

/* ==================== MIDDLEWARE USING START ==================== */
app.use(xmlParser())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(passport.initialize())
app.use(helmet())
app.use(morgan('combined'))
/* ==================== MIDDLEWARE USING END ==================== */

/* ==================== ROUTES USE START ==================== */
app.use('/test', TestRouter)
app.use('/validation', ValidationRouter)
/* ==================== ROUTES USE END ==================== */

/*==================== SERVER START ====================*/
app.set('port', PORT)
app.listen(PORT, () => {})
/*==================== SERVER END ====================*/
